#!/usr/bin/env python3

import math
import random

import splat.interpol

debug = False


def dump(vector, do_print=False):
    txt = ' '.join(f"{value:.3f}" for value in vector)
    if do_print:
        print(txt)
    return txt


class Layer:

    def __init__(self, size, parent=None):
        self.size = size
        self.parent = parent
        self.neurons = [0.0] * size
        self.weights = [
            [random.random()] * parent.size if parent is not None else []
            for n in range(size)
        ]
        self.bias = [0.0 for n in range(size)]

    @classmethod
    def sigmoid(cls, value):
        return 1 / (1 + math.exp(-value))

    def _check_size(self, values):
        if len(values) != self.size:
            raise ValueError(f"Wrong size: {len(values)}, {self.size}")

    def set_neurons(self, values):
        self._check_size(values)
        self.neurons = values

    def set_weights(self, values):
        self._check_size(values)
        weights = []
        for old_values, new_values in zip(self.weights, values):
            if len(old_values) != len(new_values):
                raise ValueError(
                    f"Wrong size: {len(neuron)}, {len(new_values)}"
                )
            weights.append(new_values)
        self.weights = weights

    def set_bias(self, values):
        self._check_size(values)
        self.bias = values

    def compute(self, data):
        if self.parent is None:
            self.set_neurons(data)
        else:
            if len(data) != self.parent.size:
                raise ValueError(
                    f"Wrong size: {len(data)}, {self.parent.size}"
                )
            output = []
            for weights, bias in zip(self.weights, self.bias):
                products = tuple(
                    value * weight
                    for value, weight in zip(data, weights)
                )
                raw = sum(products) + bias
                result = self.sigmoid(raw)
                if debug:
                    print(f"   {dump(weights)}  {bias:.3f}")
                    print(f"   {dump(products)}  {raw:.3f} {result:.3f}")
                output.append(result)
            self.set_neurons(output)
        return self.neurons


class Network:

    def __init__(self, layers):
        self.layers = layers

    @classmethod
    def from_sizes(cls, layer_sizes):
        layers = []
        parent = None
        for size in layer_sizes:
            layer = Layer(size, parent=parent)
            layers.append(layer)
            parent = layer
        return cls(layers)

    @classmethod
    def from_init_data(cls, data):
        layers = []
        parent = None
        for weights, bias in data:
            layer = Layer(len(weights), parent)
            layer.set_weights(weights)
            layer.set_bias(bias)
            layers.append(layer)
            parent = layer
        return cls(layers)

    def compute(self, data, do_dump=False):
        for layer in self.layers:
            data = layer.compute(data)
            if do_dump:
                dump(data, True)
        return data


def gradient_descent(points, step=0.0001, ε=0.001):
    def f(x, *coefs):
        y = 0
        for deg, coef in enumerate(coefs):
            y += coef * x ** deg
        return y

    size = len(points)
    coefs = tuple(random.random() for i in range(size))
    error = list(0.0 for i in range(size))
    trail = tuple(0.0 for i in range(3))
    loop = 0
    while True:
        grad = list(0.0 for i in range(size))
        steps = list(coefs)
        for i, (x, y) in enumerate(points):
            z = f(x, *coefs)
            delta = abs(z - y)
            dx = list(coefs)
            dx[i] += ε
            dy = abs(f(x, *dx) - y)
            grad = (dy - delta) / ε
            steps[i] -= grad * step
            error[i] = delta
        trail = (sum(error) / size, trail[0], trail[1])
        if trail[0] == trail[2]:
            break
        coefs = tuple(steps)
        loop += 1
    if debug:
        print("Loops", loop)
    return coefs


def polynomial():
    #  a + b * x + c * x^2 = y
    # points: (x, y)
    # coefs: (a, b, c)
    points = [
        (1.3, 5.2),
        (4.5, -2.9),
        (5.7, 3.0),
    ]

    # interpolation
    spline = splat.interpol.spline(points)
    coefs = spline.pols[0][2].coefs
    print(coefs)

    # gradient descent approximation
    coefs2 = gradient_descent(points)
    print(coefs2)

    a, b, c = coefs
    x = 4.1
    print(x, spline.value(x), a + b*x + c*x**2)

    a, b, c = coefs2
    x = 4.1
    print(x, spline.value(x), a + b*x + c*x**2)


def nnet():
    input_size = 16
    network = Network.from_sizes([input_size, 4, 3, 2])
    input_data = [
        [
            max(min(random.random() * 4 - 1.5, 1.0), 0.0) / input_size
            for i in range(input_size)
        ]
        for j in range(32)
    ]
    training_data = [
        (
            entry,
            [1.0, 0.0] if sum(entry) > 0.5 else [0.0, 1.0]
        )
        for entry in input_data
    ]

    for entry in input_data:
        print(f"{sum(entry):.3f} -> ", end='')
        output = network.compute(entry)
        dump(output, True)


def main():
    random.seed(123)
    polynomial()
    nnet()


if __name__ == '__main__':
    main()
